var LS = LS || {};

LS.Map = {
  init: function () {
    this.kolkata = [0, 0];
    this.userId = $("#map").data("user");
  },
  initMap: function() {
    this.userId = $("#map").data("user");
    this.kolkata = ol.proj.fromLonLat([88.3639, 22.5726]);

    var attribution = new ol.control.Attribution({
      collapsible: false
    });
    this.attribution = attribution;

    var view = new ol.View({
      center: this.kolkata,
      zoom: 8
    });

    var layer = [
      new ol.layer.Tile({
        preload: 4,
        source: new ol.source.OSM()
      })
    ];


    var map = new ol.Map({
      layers: layer,
      target: 'map',
      controls: ol.control.defaults({
        attributionOptions: {
          collapsible: false
        }
      }),
      loadTilesWhileAnimating: true,
      view: view
    });

    this.map = map;

    $.each($("#map").data("locations"), function(index, location) {
      var i = index + 1;
      var coord = ol.proj.transform([location.latitude, location.longitude],'EPSG:3857', 'EPSG:4326');
      var pos = ol.proj.fromLonLat(coord);
      var marker1 = new ol.Overlay({
        position: pos,
        positioning: 'center-center',
        element: document.getElementById('marker-'+ i),
        stopEvent: false
      });
    
      map.addOverlay(marker1);

      var vienna1 = new ol.Overlay({
        position: pos,
        //element: document.getElementById('vienna'),
        element: document.getElementById('loc-'+ i)
      });
      map.addOverlay(vienna1);
    });
  /*
    var pos = ol.proj.fromLonLat([16.3725, 48.208889]);
    var marker = new ol.Overlay({
      position: pos,
      positioning: 'center-center',
      element: document.getElementById('marker'),
      stopEvent: false
    });
    
    map.addOverlay(marker);

    var vienna = new ol.Overlay({
      position: pos,
      //element: document.getElementById('vienna'),
      element: document.getElementById('vienna')
    });
    map.addOverlay(vienna);


    var coord = ol.proj.transform([9157767.484790398, 2622095.8182946863],'EPSG:3857', 'EPSG:4326');
    var pos = ol.proj.fromLonLat(coord);
    var marker1 = new ol.Overlay({
      position: pos,
      positioning: 'center-center',
      element: document.getElementById('marker-1'),
      stopEvent: false
    });
    
    map.addOverlay(marker1);

    var vienna1 = new ol.Overlay({
      position: pos,
      //element: document.getElementById('vienna'),
      element: document.getElementById('loc-1')
    });
    map.addOverlay(vienna1);

    */
    var popup = new ol.Overlay({
      element: document.getElementById('popup')
    });
    map.addOverlay(popup);

    map.on('click', function(evt) {
      var element = popup.getElement();
      var coordinate = evt.coordinate;
      var userId = $("#map").data("user");
      var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
          coordinate, 'EPSG:3857', 'EPSG:4326'));

      if(confirm("Are you sure, you want to check in to this place?")) {
        $(element).popover('destroy');
        popup.setPosition(coordinate);
      // the keys are quoted to prevent renaming in ADVANCED mode.

        $.post("/users/"+ userId +"/locations", { latitude: coordinate[0], longitude: coordinate[1]})
          .done(function(response) {
            window.location.href = '/users/'+ userId +'/locations';
          });

        $(element).popover({
          'placement': 'top',
          'animation': false,
          'html': true,
          'content': '<p>The location you clicked was:</p><code>' + hdms + '</code>'
        });
        $(element).popover('show');
      }
    });

  
    //LS.Map.checkSize();

/*
var style1 = [
  new ol.style.Style({
      image: new ol.style.Icon(({
          scale: 0.7,
          rotateWithView: false,
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          opacity: 1,
          src: '//raw.githubusercontent.com/jonataswalker/map-utils/master/images/marker.png'
      })),
      zIndex: 5
  }),
  new ol.style.Style({
      image: new ol.style.Circle({
          radius: 5,
          fill: new ol.style.Fill({
              color: 'rgba(255,255,255,1)'
          }),
          stroke: new ol.style.Stroke({
              color: 'rgba(0,0,0,1)'
          })
      })
  })
];
var sourceFeatures = new ol.source.Vector();

var feature = new ol.Feature({
  type: 'click',
  desc: long_string,
  geometry: new ol.geom.Point([-9120944.666442728, 2653259.4403269454])
});

feature.setStyle(style1);
*/

/*    map.on('click', function(evt) {
      var coordinates = evt.coordinate;
      alert(coordinates);
      });
  */
  },
  checkSize: function() {
    var small = this.map.getSize()[0] < 600;
    this.attribution.setCollapsible(small);
    this.attribution.setCollapsed(small);
  }
}

//window.addEventListener('resize', LS.Map.checkSize());

$(".share-current-location").click(function(e) {
  e.preventDefault();
  userId = $(this).data("user-id");
  LS.Modal.showRemoteModal("/users/"+ userId +"/share_locations", "Share your location", "share-w-f-modal");
});
